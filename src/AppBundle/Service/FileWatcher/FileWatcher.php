<?php

namespace AppBundle\Service\FileWatcher;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Finder\Finder;

class FileWatcher
{

    /**
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    protected $fileSystem = null;

    /**
     * @var string Root Dir to the App
     */
    protected $rootApp = null;

    /**
     * @var \Symfony\Component\Finder\Finder
     */
    protected $finder = null;

    public function __construct($rootApp, Filesystem $fileSystem, Finder $finder)
    {
        $this->fileSystem = $fileSystem;
        $this->rootApp = $rootApp;
        $this->finder = $finder;
    }

    protected function getFinder()
    {
        return $this->finder;
    }

    protected function getFileSystem()
    {
        return $this->fileSystem;
    }

    protected function root()
    {
        return $this->rootApp;
    }

    /**
     * Create Folder if not exists
     * @param $path 'Path to the logs'
     * @return void
     */
    protected function existsOrCreate($path) {

        if (!$this->getFileSystem()->exists($path)) {
            try {
                $this->getFileSystem()->mkdir($path, 0777);
            } catch (IOExceptionInterface $exception) {
                dump($exception->getMessage());
            }
        }

    }

}