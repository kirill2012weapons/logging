<?php

namespace AppBundle\Service\FileWatcher;


use Symfony\Component\Filesystem\Filesystem;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Kassner\LogParser\LogParser;

class LoggerFileWatcher extends FileWatcher
{

    public function createFileLog($path, $fileName)
    {

        $this->existsOrCreate($path);
        $this->getFileSystem()
            ->touch(
                $path . '/' . $fileName . '.log'
            );

        return $path . '/' . $fileName . '.log';
    }

    public function getLogFiles($path)
    {
        $fileSys = $this->getFileSystem();
        $fileRoot = $path;
        $finder = $this->getFinder();

        $massFiles = [];

        $finderLogs = $finder
            ->files()
            ->sortByModifiedTime()
            ->in($fileRoot);

        foreach ($finderLogs as $file) {
            $massFiles[] = [
                'files' => $file
            ];
        }

        return array_reverse($massFiles);
    }

    public function getCurrentLogFile($name, $path)
    {
        $fileSys = $this->getFileSystem();
        if (!$fileSys->exists($path . '/' . $name)) return false;

        $parser = new LogParser();

        $lines = file($path . '/' . $name, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $outputLines = [];
        foreach ($lines as $line) {
            $outputLines[] = $line;
        }
        return $outputLines;
    }

}