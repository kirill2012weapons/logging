<?php

namespace AppBundle\Service;


use AppBundle\Entity\LogFile;
use AppBundle\Service\FileWatcher\LoggerFileWatcher;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class MyLogger
{

    private $path = null;

    /**
     * @var LoggerFileWatcher|null
     */
    private $loggerFileWatcher = null;
    /**
     * @var Logger|null
     */
    private $monolog = null;
    /**
     * @var StreamHandler|null
     */
    private $streamHandler = null;

    public function __construct($path, LoggerFileWatcher $loggerFileWatcher)
    {
        $this->path = $path;
        $this->loggerFileWatcher = $loggerFileWatcher;
    }

    private function getPath()
    {
        return $this->path;
    }

    private function getLoggerFileWatcher()
    {
        return $this->loggerFileWatcher;
    }

    private function getLogger()
    {
        return $this->monolog;
    }

    private function createLogFile($fileName)
    {
        return $this->getLoggerFileWatcher()
            ->createFileLog($this->getPath(), $fileName);
    }

    public function setLogToFile($arr)
    {
        if (!isset($arr['name'])) return;
        $path = $this->createLogFile($arr['name']);

        $log = new Logger(strtoupper($arr['name']));
        $log->pushHandler(new StreamHandler($path, Logger::INFO));
        $str = '';
        foreach ($arr as $key => $value) {
            $str .= $key . ' - ' . $value . '; ';
        }
        $log->addInfo($str);

    }

    public function getLogFiles()
    {
        $path = $this->getPath();
        $log = $this->getLoggerFileWatcher();

        $files = $log->getLogFiles($path);

        $filesWithInfo = [];

        foreach ($files as $file) $filesWithInfo[] = new LogFile($file['files']);

        return $filesWithInfo;
    }

    public function getCurrentLogFile($fileName)
    {
        $path = $this->getPath();

        $loggerFileWatcher = $this->getLoggerFileWatcher();
        return $loggerFileWatcher->getCurrentLogFile($fileName, $path);
    }

}