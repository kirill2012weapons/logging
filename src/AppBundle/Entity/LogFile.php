<?php

namespace AppBundle\Entity;

use Symfony\Component\Finder\SplFileInfo;

class LogFile
{

    private $name = null;

    private $type = null;

    private $sizeNumber = null;
    private $size = null;

    private $dateCreated = null;
    private $dateCreatedNumber = null;

    public function __construct(SplFileInfo $fileInfo)
    {
        $this->setName( $fileInfo->getFilename() );
        $this->setType( strtoupper($fileInfo->getExtension()) );
        $this->setSize( $fileInfo->getSize() );
        $this->setDateCreated( $fileInfo->getMTime() );
    }

    /**
     * Setter an Getter Fo Name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    public function getName()
    {
        return $this->name;
    }

    /**
     * Setter an Getter Fo Type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
    public function getType()
    {
        return $this->type;
    }

    /**
     * Setter an Getter Fo Name
     */
    public function setSize($size)
    {
        $this->size = $this->readebleBytes($size);
        $this->sizeNumber = $size;
    }
    public function getSize()
    {
        return $this->size;
    }
    public function getSizeNumber()
    {
        return $this->sizeNumber;
    }

    /**
     * Setter an Getter Fo DateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated =gmdate("Y-m-d H:i:s", $dateCreated);
        $this->dateCreatedNumber = $dateCreated;
    }
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    private function readebleBytes($bytes)
    {
        $i = floor(log($bytes) / log(1024));
        $sizes = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');

        return sprintf('%.02F', $bytes / pow(1024, $i)) * 1 . ' ' . $sizes[$i];
    }
}