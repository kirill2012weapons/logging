<?php

namespace AppBundle\Controller;

use AppBundle\Service\FileWatcher\FileWatcher;
use AppBundle\Service\FileWatcher\LoggerFileWatcher;
use AppBundle\Service\FileWatcher\LoggerWatcher;
use AppBundle\Service\MyLogger;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LogsController extends Controller
{

    /**
     * @Route(
     *     "/logs",
     *     name="logs_index"
     * )
     */
    public function indexAction(Request $request, MyLogger $myLogger)
    {
        return $this->render('logs/index.html.twig', [
            'files' => $myLogger->getLogFiles(),
        ]);
    }

    /**
     * @Route(
     *     "/logs/{name}",
     *     name="logs_current"
     * )
     */
    public function showCurrentAction(Request $request, MyLogger $myLogger, $name)
    {
        if (empty($name)) return $this->redirectToRoute('logs_index');

        $lines = $myLogger->getCurrentLogFile($name);

        if (!$lines) return $this->redirectToRoute('logs_index');

        return $this->render('logs/show_current.html.twig', [
            'lines' => $lines,
            'fileName' => $name,
        ]);
    }

}
