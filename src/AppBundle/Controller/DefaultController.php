<?php

namespace AppBundle\Controller;

use AppBundle\Service\FileWatcher\FileWatcher;
use AppBundle\Service\FileWatcher\LoggerFileWatcher;
use AppBundle\Service\FileWatcher\LoggerWatcher;
use AppBundle\Service\MyLogger;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{

    /**
     * @Route(
     *     "/",
     *     name="homepage_index_set_logs"
     * )
     */
    public function indexAction(Request $request, MyLogger $myLogger)
    {

        $myLogger->setLogToFile(
            $request->query->all()
        );

        if(count($request->query->all()) == 0) {
            return $this->render('default/index.html.twig');
        }

        return $this->render('default/index_req.html.twig');

    }

}
